#!/usr/bin/env python3
import sys,os,re
import subprocess
from datetime import datetime
EXP = "experiments"
START = "start_position"
def usage():
    print("msa-limit: Launch the MSA analysis pipeline\n",
        "Usage:",
        'msa-limit.py -i <file_reads> -r <file_ref> [-options]',
        'msa-limit.py [mode]\n',
        "Arguments: ",
        "  required:",
        "    -i <string>",
        "       nanopore long reads file (fasta or fastq)",
        "    -r <string>",
        "       reference sequence file (fasta, a single sequence)",
        "       IUPAC consensus sequence in the diploid case\n",

        "  optional:",
        "    -c <int>",
        "       default: 24",
        "       number of cores",
        "    -n <string>",
        "       default: date and time of execution",
        "       name of the experiment",
        "    -o <int> ",
        "       default: 10",
        "       number of start positions to be tested",
        "    -b <int>,<int>,...",
        "       start position(s) (replacing -o)",
        "    -d <int>,<int>,...",
        "       default: 10,20,50",
        "       sequencing depth(s) (number of reads)",
        "    -s <int>,<int>,...",
        "       default: 100,200",
        "       size(s) of region(s)",
        "    -t <int>,<int>,... ",
        "       default: 50",
        "       threshold(s) for sequences consensus",
        "    -m <string>,<string>,...",
        "       default: muscle,mafft,poa,kalign,spoa,kalign3,clustalo,abpoa,tcoffee (all)",
        "       MSA software(s) to run",
        "    -h",
        "       help",
        "\nEx: ./msa-limit.py -i reads.fastq -r ref.fasta -b 1,15000 -n exp -d 10,100 -s 100,200 -t 50,75 -m mafft,poa",
        "\nOther modes: ./msa-limit.py [mode]",
        "  test ",
        "      Launches a pipeline test",
        "  list ",
        "      List of existing experiments",
        "  summary",
        "      More readable summary of experiments for a human",
        "        optional: ",
        "         -n <string> <string> <string> ...",
        "            default: all the names of the experiments",
        "            names of the experiments you want to display in the summary.",
        "         -p <string> ",
        "            default: nothing",
        "            Prefix for results folders.",
        "         -d ",
        "            default: false",
        "            Show diploid metrics.",
        "  run_config <string> <string> ...",
        "       Launches the pipeline from configuration file(s)",
        "        required: path to the configuration file(s).",
        "        optional: ",
        "         -c <int>",
        "            default: 24",
        "            number of cores",
        "  diploid_metric",
        "     Calculates more advanced metrics for the diploid case",
        "        optional: ",
        "         -n <string> <string> <string> ...",
        "            default: all the names of the experiments",
        "            names of the experiments for which you want to calculate the metrics.",
        "  rulegraph",
        "       Displays a graph of the snakemake rules"
        ,sep="\n")
    sys.exit()

def config_conda():
    try:
        open("src/config_conda.sh")
    except:
        result = subprocess.run("./src/create_config_conda.sh")
        try:
            open("src/config_conda.sh")
        except:
            print("Unable to launch the pipeline.")
            sys.exit()

def test():
    if not os.path.exists("test_data"):
        print("Preparation of the test data set")
        result = subprocess.run("./src/test_data.sh")
    result = subprocess.run(["./src/snakemake_launcher.sh","configuration_files/test.yaml"])

def experiments_list():
    if os.path.exists(EXP):
        result = subprocess.run("ls " + EXP,shell=True)
    else:
        print("No experiment has been launched yet")

def summary():
    if os.path.exists(EXP):
        try:
            next_name=sys.argv[sys.argv.index("-n")+1]
            i=1
            end_names = 0
            exp_names = ""
            while ( end_names == 0 and re.search("-", next_name) == None ):
                exp_names=exp_names +" "+ next_name
                i+=1
                try:
                    next_name = sys.argv[sys.argv.index("-n")+i]
                except:
                    end_names = 1
        except:
            result = subprocess.Popen("ls " + EXP,shell=True,stdout=subprocess.PIPE)
            exp_names=result.communicate()[0].decode("utf-8")
            exp_names=re.sub('\n', r' ', exp_names)
        try:
            prefix=sys.argv[sys.argv.index("-p")+1]
            prefix=prefix+"_"
        except:
            prefix=""

        if os.path.exists(prefix+"results_mean"):
            result = subprocess.run("rm -r "+prefix+"results_mean",shell=True)
        if os.path.exists(prefix+"results_all_start_positions"):
            result = subprocess.run("rm -r "+prefix+"results_all_start_positions",shell=True)
        result = subprocess.run("./src/total_data_format.py -o "+prefix+"results -n " + exp_names,shell=True)
        result = subprocess.run("./src/total_data_format.py -o "+prefix+"results -m -n " + exp_names,shell=True)
        try:
            a = sys.argv[sys.argv.index("-d")]
            result = subprocess.run("./src/total_data_format.py -o "+prefix+"results -d -n " + exp_names,shell=True)
        except:
            pass
        print("See folders: "+prefix+"results_mean & "+prefix+"results_all_start_positions")
    else:
        print("No experiment has been launched yet")

def run_config():
    cores=""
    try:
        index_c=sys.argv.index("-c")
        cores=sys.argv[index_c+1]
        del sys.argv[index_c+1]
        del sys.argv[index_c]
    except:
        pass
    try:
        path_config = sys.argv[2]
    except:
        print("ERROR: The configuration file(s) is missing.")
        print("Usage: msa_limit.py run_config <path config file>.")
        sys.exit()
    i=2
    end_files = 0
    while ( not end_files  ):
        result = subprocess.run(["./src/snakemake_launcher.sh",path_config,cores])
        i+=1
        try:
            path_config = sys.argv[i]
        except:
            end_files = 1

def diploid_metric():
    if os.path.exists(EXP):
        try:
            next_name=sys.argv[sys.argv.index("-n")+1]
            i=1
            end_names = 0
            exp_names = []
            while ( end_names == 0 and re.search("-", next_name) == None ):
                if not os.path.exists(os.path.join(EXP,next_name)):
                    print("ERROR: The name " + next_name + " isn't the name of an experiment")
                else:
                    exp_names.append(next_name)
                i+=1
                try:
                    next_name = sys.argv[sys.argv.index("-n")+i]
                except:
                    end_names = 1
        except:
            result = subprocess.Popen("ls " + EXP,shell=True,stdout=subprocess.PIPE)
            names=result.communicate()[0].decode("utf-8")
            names=re.sub('\n', r' ', names)
            exp_names=names.split(" ")
        for exp_name in exp_names:
            result = subprocess.Popen("ls " + os.path.join(EXP,exp_name) +"|grep "+ START ,shell=True,stdout=subprocess.PIPE)
            dir=result.communicate()[0].decode("utf-8")
            dir_start=dir.split("\n")
            dir_start.pop()
            threshold=""
            output_files={}
            for start in dir_start:
                result = subprocess.Popen("ls " + os.path.join(EXP,exp_name,start,"seq_consensus")+"|grep align" ,shell=True,stdout=subprocess.PIPE)
                files=result.communicate()[0].decode("utf-8")
                p = re.compile('_t[0-9]+_')
                thresholds=set(p.findall(files))
                files=files.split('\n')
                files.pop()
                for threshold in thresholds:
                    if threshold not in output_files:
                        output_files[threshold]=""
                    threshold_files=[]
                    for file in files:
                        p = re.compile(threshold)
                        if (p.search(file)):
                            threshold_files.append(os.path.join(EXP,exp_name,start,"seq_consensus",file))
                    output_file=os.path.join(EXP,exp_name,start,"results",exp_name+"_diploid_data_align"+threshold[0:-1]+".csv")
                    output_files[threshold]+=output_file + " "
                    order=["./src/diploid_metric.py",'-in']
                    for file in threshold_files:
                        order.append(file)
                    order.append("-o")
                    order.append(output_file)
                    result = subprocess.run(order)
            for threshold in thresholds:
                output_mean=os.path.join(EXP,exp_name,"results",exp_name+"_diploid_data_align"+threshold[0:-1]+".csv")
                result = subprocess.run(["./src/region_mean_launcher.sh","-in ",output_files[threshold]
                ," -out ",output_mean," -t ",threshold[2:-1]])
        print("Data generated. You will find them in the experiments results folder.")



    else:
        print("No experiment has been launched yet")
def rulegraph():
    if not os.path.exists("test_data"):
        print("Preparation of the test data set")
        result = subprocess.run("./src/test_data.sh")
    result = subprocess.run(["./src/snakemake_rulegraph.sh"])

###############################
#        Main
###############################

config_conda()

try:
    mode = sys.argv[1]
except:
    usage()

try:
    help = sys.argv.index("-h")
except:
    help=-1
if(help > -1):
    usage()

if mode == "test":
    test()
elif mode == "list":
    experiments_list()
elif mode == "summary":
    summary()
elif mode == "run_config":
    run_config()
elif mode == "diploid_metric":
    diploid_metric()
elif mode == "rulegraph":
    rulegraph()
else:
    lines=[]
    try:
        ref_file = sys.argv[sys.argv.index("-r")+1]
        ref_file = os.path.abspath(ref_file)
    except:
        print("ERROR: The input reference sequence file is missing.")
        usage()

    try:
        ref = open(ref_file, "r")
    except:
        print("ERROR: Cannot read the reference file.")
        sys.exit()

    nb_seq = 0
    for line in ref.readlines():
        if (line[0] == ">"):
            nb_seq = nb_seq + 1
    ref.close()
    if(nb_seq != 1):
        print("ERROR: The reference file must contain only one sequence.(IUPAC possible in the diploid case)")
        sys.exit()

    try:
        reads_file = sys.argv[sys.argv.index("-i")+1]
        reads_file = os.path.abspath(reads_file)
    except:
        print("ERROR: The input reads sequences file is missing.")
        usage()

    try:
        read = open(reads_file, "r")
        read.close()
    except:
        print("ERROR: Cannot read the reads file.")
        sys.exit()

    try:
        exp_name = sys.argv[sys.argv.index("-n")+1]
    except:
        now = datetime.now()
        exp_name = now.strftime("%d%m%Y%H%M%S")
        print("Name of the experiment: " + exp_name)

    print(reads_file,ref_file)
    lines.append("N: " + exp_name)
    lines.append("I: " + reads_file)
    lines.append("R: " + ref_file)

    try:
        region_sizes = sys.argv[sys.argv.index("-s")+1]
        lines.append("S: [" + region_sizes + "]")
    except:
        pass

    try:
        depths = sys.argv[sys.argv.index("-d")+1]
        lines.append("D: [" + depths + "]")
    except:
        pass

    try:
        thresholds = sys.argv[sys.argv.index("-t")+1]
        lines.append("T: [" + thresholds + "]")
    except:
        pass

    try:
        msas = sys.argv[sys.argv.index("-m")+1]
        lines.append("M: [" + msas + "]")
    except:
        pass

    try:
        beginings = sys.argv[sys.argv.index("-b")+1]
        lines.append("B: [" + beginings + "]")
    except:
        try:
            nb_start_position = sys.argv[sys.argv.index("-o")+1]
            lines.append("O: [" + nb_start_position + "]")
        except:
            pass
    cores=""
    try:
        cores = sys.argv[sys.argv.index("-c")+1]
    except:
        pass

    fichier = open(os.path.join("configuration_files",exp_name + ".yaml"), "w")
    fichier.write('\n'.join(lines))
    fichier.close()
    result = subprocess.run(["./src/snakemake_launcher.sh",os.path.join("configuration_files",exp_name + ".yaml"),cores])
