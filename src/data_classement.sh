ATTRIBUTS="number_Identity number_Match time memory"
if [[ ! -d classement ]]; then
  mkdir classement
fi
if [[ ! -d classement/data ]]; then
  mkdir classement/data
fi
for a in $ATTRIBUTS
  do  
  if [[ ! -d classement/$a ]]; then
    mkdir classement/$a
    mkdir classement/$a/data
    mkdir classement/$a/top_$a
  fi
done
for t in 50 70
  do
  for e in `ls experiments| grep -Pv "(clustal|tcoffee)"`
    do
      for s in `ls experiments/$e/|grep start_position`
        do
          if [[ -e  experiments/$e/$s/results/$e\_data_align_t$t.csv ]];then
            for a in $ATTRIBUTS
              do
                num=`head -1 experiments/$e/$s/results/$e\_data_align_t$t.csv | sed "s/,/\n/g"| grep -n $a| cut -d: -f1`
                cat experiments/$e/$s/results/$e\_data_align_t$t.csv|cut -d"," -f 1-3,$num > classement/$a/data/$e\_$s\_t$t\_$a.csv
            done
          fi
      done
   done
done

for t in 50 70
  do
  for e in `ls experiments| grep -Pv "(clustal|tcoffee)"`
    do
    for a in $ATTRIBUTS
      do
      files=`ls classement/$a/data| grep _t$t | grep $e\_start`
      files_paths=`echo $files| sed "s/ / classement\/$a\/data\//g;s/^/classement\/$a\/data\//g"`
      ./src/classement.pl $files_paths >classement/$a/top_$a/$e\_data_$t\_$a.csv
    done
  done
done
