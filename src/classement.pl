#!/usr/bin/perl -w
$nb_file=@ARGV;
my %data=();
my %result=();
my @tab;
foreach $k ( sort keys(%h))
  {
    print("{$k}: [");
    $ref=$h{$k};
    foreach $i (@$ref){
      print "$i ";
    }
    print"]\n";
  }

for (my $i = 0; $i < $nb_file; $i++) {
  $file=$ARGV[$i];
  open(IN, "<", "$file");
  while (<IN>)
  {
    chomp;
    unless(/^MSA/){
      ($msa,$region,$depth,$match)=split(/,/,$_);
      $data{$region}{$depth}{$msa}=$match;
    }
  }
  close IN;
  foreach $region (sort( {$a<=>$b} keys(%data)))
  {
    foreach $depth ( sort( {$a<=>$b}keys(%{$data{$region}})))
    {
      @tab=();
      foreach $msa ( sort keys(%{$data{$region}{$depth}}))
      {
        push(@tab,$data{$region}{$depth}{$msa});
      }
      @tab=reverse sort ( {$a<=>$b} @tab);
      foreach $msa ( sort keys(%{$data{$region}{$depth}}))
      {
        $j=0;
        while ($data{$region}{$depth}{$msa} != $tab[$j] ) {
          $j++;
        }
        $j++;
         if (! exists$result{$region}{$depth}{$msa}){
           for (my $k = 1; $k <= @tab; $k++) {
             $result{$region}{$depth}{$msa}{$k}=0;
           }
         }
         $result{$region}{$depth}{$msa}{$j}++;
      }
    }
  }
}

print("MSA,region_size,depth");
for (my $j = 1; $j <= @tab; $j++){
  print(",$j");
}
print("\n");
foreach $region (sort( {$a<=>$b} keys(%result)))
{
  foreach $depth ( sort( {$a<=>$b}keys(%{$result{$region}})))
  {
    foreach $msa ( sort keys(%{$result{$region}{$depth}}))
    {
      print("$region,$depth,$msa");
      foreach $j ( sort( {$a<=>$b}keys(%{$result{$region}{$depth}{$msa}})))
      {
        if ($j != 0){
          print(",$result{$region}{$depth}{$msa}{$j}");
        }
      }
      print("\n");
    }
  }
}
