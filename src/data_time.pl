#!/usr/bin/perl -w
sub time_converter {
  my ($time) = @_;
  @tab = split(/:/,$time);
  $nb_val = @tab;
  if ($nb_val >= 3){$time = $time.":0";}
  else{$time = "0:".$time;}
  ($h,$m,$s) = split(/:/,$time);
  $time = 60*60*$h + 60*$m + $s;
  return $time;
}

print("MSA,base,read,user,system,time,elapsed,cpu,memory,rtime_memory\n");
foreach $file (@ARGV)
{
  open IN,$file;

  @path = split(/\//,$file);
  $name_file = pop(@path);
  @tab_name_file = split(/_/,$name_file);
  $nb_read = pop(@tab_name_file);
  $base_pair = pop(@tab_name_file);
  $msa=pop(@tab_name_file);

  while (<IN>){
    if (/user/){
      chomp;
      s/user//,s/system//,s/elapsed//,s/%CPU//,s/maxresident\)k//;
      ($time_user,$time_system,
       $time_elapsed,$cpu,$avg,$maxresident) = split(/ /,$_);
       $time_elapsed = time_converter($time_elapsed);
       $time_user_system = $time_user + $time_system;
       $time_memory=$time_user_system/$maxresident;

       $base_pair =~ s/r//;
       $nb_read =~ s/d//;
       print("$msa,$base_pair,$nb_read,$time_user,$time_system,$time_user_system,".
             "$time_elapsed,$cpu,$maxresident,$time_memory\n")
    }
  }

  close IN;
}
$avg=0;
