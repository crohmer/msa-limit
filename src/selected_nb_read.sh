#!/bin/bash
if [ "$#" -lt 3 ]
then
    echo "USAGE: ./selected_nb_read.sh file nb_read output_directory"
else
    FILE=$1
    NB_READ=$2
    OUTPUT=$3
    let "LINE = $NB_READ*2"
    if [ `head -$LINE $FILE|wc -l` -eq $LINE ]
    then
        head -$LINE $FILE
    else
        echo "ERROR selected_nb_read: Not enough reads to get $NB_READ reads for $FILE" >&2
    fi
fi
