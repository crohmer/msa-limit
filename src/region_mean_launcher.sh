#!/bin/bash
source ./src/config_conda.sh
if [[ ! -d .conda_python3 ]]; then
  conda env create -p .conda_python3 -f env_conda/python3.yaml  >/dev/null
fi
CURRENT_PATH=`pwd`
conda activate $CURRENT_PATH/.conda_python3

./src/region_mean.py $@
