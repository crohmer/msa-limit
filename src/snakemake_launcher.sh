#!/bin/bash
source ./src/config_conda.sh
if [[ ! -d .conda_snakemake ]]; then
  echo "Create a conda env for snakemake"
  conda env create -p .conda_snakemake -f env_conda/snakemake.yaml  >/dev/null
fi
CURRENT_PATH=`pwd`
conda activate $CURRENT_PATH/.conda_snakemake

CONFIG_EXPERIMENT=$1
if [[ $2 == "" ]]; then
  CORES="24"
else
  CORES=$2
fi
echo "Launch Snakemake with $CONFIG_EXPERIMENT"
snakemake --configfile $CONFIG_EXPERIMENT -c$CORES --use-conda --rerun-incomplete --keep-going
