#!/usr/bin/env python3
import sys,csv
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from random import random, choice
from pylab import *
plt.style.use('seaborn-whitegrid')
import numpy as np
def recovered_threshold(file):
  path=file.split("_")[-1].split(".")[0][1:]
  return path

attribute=sys.argv[1]
files=sys.argv[3:]
out=sys.argv[2]
files=sys.argv[3:]
threshold_independent=1
if (attribute =="time" or attribute =="elapsed" or attribute =="memory"):
  files=[files[0]]
  threshold_independent=0
data={}
attribute_max="a"
depths=[]
region_sizes=[]
empty_file=True
for file in files:
  threshold=recovered_threshold(file)
  with open(file, newline='') as csvfile:
    data[threshold]={}
    spamreader=csv.reader(csvfile)
    titles={}
    for row in spamreader:
      if row[0]== "MSA":
        for i in range(len(row)):
          titles[row[i]]=i
      else:
        empty_file=False
        msa=row[titles["MSA"]]
        region_size=row[titles["region_size"]]
        depth=int(row[titles["depth"]])
        if msa not in data[threshold]:
          data[threshold][msa]={}
        if region_size not in data[threshold][msa]:
          data[threshold][msa][region_size]={}
          if region_size not in region_sizes:
              region_sizes.append(region_size)
        if depth not in depths:
          depths.append(depth)
        val=float(row[titles[attribute]])
        data[threshold][msa][region_size][depth]=val
        if attribute_max=="a" or val > attribute_max:
          attribute_max=val
if empty_file == False :
  depths=sort(depths)
  region_sizes=sort(region_sizes)
  min_depth=depths[0]
  max_depth=depths[-1]
  colors=['b','g','r','c','m','y']
  len_regions=len(region_sizes)
  if len_regions>len(colors):
      all_colors=list(matplotlib.colors.cnames.keys())
      random()
      while len_regions>len(colors):
          colors.append(choice(all_colors))
  pp = PdfPages(out)
  for threshold in data:
    for msa in data[threshold]:
      fig = plt.figure()
      ax = plt.axes()
      ax = ax.set(xlabel='Depth', ylabel=attribute)
      if (threshold_independent==0):
          threshold_display=""
      else:
          threshold_display=" (Threshold: " + threshold+")"

      plt.title(attribute + " for " + msa + threshold_display)
      i=0
      plt.axis([min_depth, max_depth,0, float(attribute_max)+5]);
      for region_size in data[threshold][msa]:
        x=[]
        y=[]
        for depth in depths:
            if depth in data[threshold][msa][region_size]:
                x.append(depth)
                y.append(data[threshold][msa][region_size][depth])
        x = np.array(x)
        y = np.array(y)
        plt.scatter(x,y,color=colors[i])
        plt.plot(x, y, color=colors[i], linestyle='solid', label=" Region size: " + region_size)
        i+=1
      plt.legend();
      pp.savefig(fig)
      plt.close()
  pp.close()
else:
  pp = PdfPages(out)
  fig = plt.figure()
  plt.title("No data available")
  pp.savefig(fig)
  print("No data available")
  pp.close()
