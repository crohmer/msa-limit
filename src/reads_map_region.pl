#!/usr/bin/perl -w
use List::Util qw(min max);

#------------------------------------------------------------------------------
# Usage check 
#------------------------------------------------------------------------------
sub usage{
	print "\nRead_map_region uses a SAM file to retrieve in FASTA format the reads\n".
		  "mapped to a specific region of the reference. Reads are truncated to\n".
		  "retrieve only the mapped section but this step can be removed with the\n".
			"uncut-end option.\n".
		  "\nUSAGE: ./read_map_region.pl [option] -s <int> -t <int> <in.sam> <output>\n".
	      "\t-s starting position of the region\n".
	      "\t-t size of the region\n".
	      "\t-e end position of the region (This option replaces the -t)\n".
	      "\nOPTION(S):\n".
	      "\t-c percentage of coverage from which the read is kept (default: 100)\n".
	      "\t-u --uncut-end (always put first) do not cut the ends of the read\n".
				"\t                not mapped to the region\n".
	      "\n";
	exit;
}

if (!exists $ARGV[0] || $ARGV[0] =~ '-h'){
	usage();
}

if ( $ARGV[0] =~ /(-u|--uncut-end)/ ){
	$UNCUT_END = 1;
	shift(@ARGV);
}
else{
	$UNCUT_END = 0;
}

$OUTPUT = pop(@ARGV);
$FILE_INPUT = pop(@ARGV);
@TAB_FILE_INPUT = split(/\./,$FILE_INPUT);
$FORMAT = pop(@TAB_FILE_INPUT);
if ( !open(IN, "<", $FILE_INPUT) || $FORMAT ne 'sam' ){
	print "The file could not be opened or the format is incorrect.\n";
	usage();
}

%hash_argv = @ARGV;

if ( (exists $hash_argv{'-s'}) && ($hash_argv{'-s'} =~ /[0-9]+/) ) {
    $REF_START = $hash_argv{'-s'}*1;
}
else{
    print "The beginning of the region must be indicated.\n";
    usage();
}

if ( (exists $hash_argv{'-t'}) && ($hash_argv{'-t'} =~ /[0-9]+/) ){
    $REGION_SIZE = $hash_argv{'-t'}*1;
    $REF_END = $REF_START + $REGION_SIZE - 1;
}
else{
	if ( (exists $hash_argv{'-e'}) && ($hash_argv{'-e'} =~ /[0-9]+/) ){
		$REF_END = $hash_argv{'-e'}*1;
		$REGION_SIZE = $REF_END - $REF_START + 1;
	}
	else{
		print "The size of the region or the end of the region must be indicated.\n";
		usage();
	}
}

if (exists $hash_argv{'-c'}){
	$COVERAGE = $hash_argv{'-c'};
	if ($COVERAGE > 100 || $COVERAGE < 0){
		print "The coverage must be between 0 and 100.\n";
		usage();
	}
}
else{
	$COVERAGE = 100;
}

#------------------------------------------------------------------------------
# START OF THE SCRIPT
#------------------------------------------------------------------------------

#Function: Returns true if the flag is present in the byte
#Arg: $FLAG int flag to test
#			$power int byte power search
#Return: bool
sub checksflag
{
	my ($FLAG,$power)=@_;
	return ((($FLAG/(2**$power)) % 2) != 0);
}

open(OUT, ">", $OUTPUT) || die ("You cannot create the file $OUTPUT");
#Reading the sam file
while (<IN>)
{
  unless(/^@/)
  {
  	chomp;
    ($QNAME,$FLAG,$RNAME,$pos_start,$MAPQ,$CIGAR,$RNEXT,$PNEXT,$TLEN,$SEQ) = split(/\t/,$_);

		#Those who match and have a primary alignment
		if (!checksflag($FLAG,2) && !checksflag($FLAG,8)){
	    #Those who match before the end of the region
	    if ($pos_start <= $REF_END) {
	      @nb_cigar = split(/[A-Z]/,$CIGAR);
	      @op_cigar = split(/[0-9]*/,$CIGAR);
	      shift(@op_cigar);
	      $nb_op_cigar = @op_cigar;
	      $pos_end = $pos_start-1;#The first nucleotide is used
	      $i = 0;
	      $nb_insertion = 0;
	      $nb_deletion = 0;
				$nb_segment = 0;
	      $read_finish = 0;
	      #Read that starts before the region
	      if ($pos_start < $REF_START){
					#As long as the cigar is not finished or the beginning of the region is not over
					while ($i < $nb_op_cigar && $pos_end < $REF_START-1) {
						if (!($op_cigar[$i] =~ "H" )) {
							if ($op_cigar[$i] =~ 'I'){
								$nb_insertion += $nb_cigar[$i];
							}
							else{
								if ($op_cigar[$i] =~ 'S'){
									$nb_segment += $nb_cigar[$i];
								}
								else{
									$pos_end += $nb_cigar[$i];
									if ($op_cigar[$i] =~ 'D'){
									$nb_deletion += $nb_cigar[$i];
									}
								}
							}
						}
						$i++;
					}

					$diff_pressure_cigar = $pos_end - ($REF_START -1) ;

					#If the cut is not made between two operators of the cigar
					if ($diff_pressure_cigar > 0){
						$i--;
						$pos_end -= $diff_pressure_cigar;
						$nb_cigar[$i] = $diff_pressure_cigar;
						$nb_deletion -= $diff_pressure_cigar if ($op_cigar[$i] =~ 'D');
					}else{
						#If the read is not finished
						if ($i < $nb_op_cigar){
							#If there is an insertion on the next operator
							#to be able to cut after
							if ($op_cigar[$i] =~ 'I'){
								$nb_insertion += $nb_cigar[$i];
								$i++;
							}
						}
						#Otherwise, the read match before the region
						else{
							$read_finish = 1;
						}
					}


					$start_match_on_read = $pos_end - $pos_start + $nb_insertion + $nb_segment - $nb_deletion +1;

	      }else{
	        $start_match_on_read=0;
	      }

	      #Eliminate those who finish before the region
	      #after this condition all reads are mapped to the region
	      if (!$read_finish) {
					#Continues the course of the cigar, stops if the fn of the region is met
	        while ($i < $nb_op_cigar && $pos_end < $REF_END) {
						if (!($op_cigar[$i] =~ 'H')) {
		          if ($op_cigar[$i] =~ 'I'){
		            $nb_insertion += $nb_cigar[$i];
		          }
		          else{
								if ($op_cigar[$i] =~ 'S'){
									$nb_segment += $nb_cigar[$i];
								}
								else{
									$pos_end += $nb_cigar[$i];

									if ($op_cigar[$i] =~ 'D'){
									$nb_deletion += $nb_cigar[$i];
									}
								}
		          }
						}
	          $i++;
	        }
					#If the cut is not made between two operators of the cigar
					$diff_pressure_cigar = $pos_end - $REF_END ;
					if ($diff_pressure_cigar > 0){
						$i--;
						$pos_end -= $diff_pressure_cigar;
						$nb_cigar[$i] = $diff_pressure_cigar;
						$nb_deletion -= $diff_pressure_cigar if ($op_cigar[$i] =~ 'D');
					}
					$end_match_on_read = $pos_end - $pos_start + $nb_insertion + $nb_segment - $nb_deletion;

	        $region_coverage = max($REF_END,$pos_end) - min($REF_START,$pos_start) - abs($REF_END - $pos_end) - abs($REF_START - $pos_start) + 1;
					if ($region_coverage >= $REGION_SIZE*($COVERAGE/100) ) {
	        	if ($UNCUT_END){
							print OUT ">$QNAME\n";
	        		print OUT "$SEQ\n";
	        	}
	        	else{
							$truncated_seq_size = $end_match_on_read - $start_match_on_read;
							if ($truncated_seq_size > $REGION_SIZE/2) {
								print OUT ">$QNAME\n";
								@seq = split(//,$SEQ);
								for (my $j = $start_match_on_read; $j <= $end_match_on_read; $j++) {
									print OUT "$seq[$j]";
								}
								print OUT "\n";
							}
			      }
	        }
	      }
	    }
		}
  }
}
close IN;

$MAPQ=$PNEXT=$RNAME=$TLEN=$RNEXT=0;
