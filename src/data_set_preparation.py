#!/usr/bin/env python3
import sys,re,os

try:
    input_read = sys.argv[sys.argv.index("-i")+1]
    input_ref = sys.argv[sys.argv.index("-i")+2]
except:
	sys.stderr.write("ERROR: The name of the input file(s) is missing.\n")

try:
    prefix = sys.argv[sys.argv.index("-p")+1]
except:
	sys.stderr.write("ERROR: The prefix for start position is missing.\n")

try:
    exp_name = sys.argv[sys.argv.index("-n")+1]
except:
	sys.stderr.write("ERROR: The name of experience is missing.\n")

try:
    starts = [sys.argv[sys.argv.index("-b")+1]]
except:
	sys.stderr.write("ERROR: The beginings is missing.\n")
else:
	i=2
	try:
		next_start = sys.argv[sys.argv.index("-b")+i]
	except:
		end_starts = 1
	else:
		end_starts = 0

	while ( end_starts == 0 and re.search("(-|>)", next_start) == None ):
		starts.append(next_start)
		i+=1
		try:
			next_start = sys.argv[sys.argv.index("-b")+i]
		except:
			end_starts = 1


order = "ln -s " + input_read + " " + exp_name + "/data/reads.fasta"
os.system(order)

order = "ln -s " + input_ref + " " + exp_name + "/data/ref.fasta"
os.system(order)


for start in starts:
    order = "echo " + start + " >" + exp_name + "/" + prefix + start + "/start_position.txt"
    os.system(order)
