#!/usr/bin/env python3
import sys,re,os,math

#Script use
def use():
	print("\nScript:\tmean_region.py",
          "Objectif: \n",
          "To Run:",
		  "./mean_region.py -in <file1> <file2> <file3> ...\n",
          "Arguments: ",
		  "  -required: -in : output script data_formatting all region.",
		  "             -out: output for region mean.",sep="\n")
	sys.exit()


try:
	files= [sys.argv[sys.argv.index("-in")+1]]
except:
	print("ERROR: The name of file(s) is missing.\n")
	use()
else:
	i=2
	try:
		next_files = sys.argv[sys.argv.index("-in")+i]
	except:
		end_files = 1
	else:
		end_files = 0

	while ( end_files == 0 and re.search("-", next_files) == None ):
		files.append(next_files)
		i+=1
		try:
			next_files = sys.argv[sys.argv.index("-in")+i]
		except:
			end_files = 1

try:
	output= sys.argv[sys.argv.index("-out")+1]
except:
	print("ERROR: The name of output is missing.\n")
	use()

try:
	threshold= sys.argv[sys.argv.index("-t")+1]
except:
	print("ERROR: The threshold is missing.\n")
	use()
data={}
data["order"]=[]
attributes=[]
first_line=""
for file in files :
	try:
		read = open(file, "r")
		for line in read.readlines():
			line = re.sub('\n', r'', line)
			if not re.search("MSA", line) :
				tab_line=line.split(",")
				msa=tab_line[0]
				region_size=tab_line[1]
				depth=tab_line[2]
				if (msa not in data):
					data[msa] = {}
					data["order"].append(msa)
					data[msa]["order"]=[]

				if (region_size not in data[msa]):
					data[msa][region_size] = {}
					data[msa]["order"].append(region_size)
					data[msa][region_size]["order"]=[]

				if (depth not in data[msa][region_size]):
					data[msa][region_size][depth] = {}
					data[msa][region_size]["order"].append(depth)
					for attribute in attributes:
						data[msa][region_size][depth][attribute]=[]

				for i in range(len(attributes)):
					data[msa][region_size][depth][attributes[i]].append(float(tab_line[i+3]))

			else:
				if first_line == "":
					pass
					attributes=line.split(",")
					first_line=attributes.pop(0)+ ","
					first_line+=attributes.pop(0)+ ","
					first_line+=attributes.pop(0)
					for attribute in attributes:
						first_line += "," + attribute + ",sd_" + attribute
		read.close()
	except:
		print(file,"don't exists.\n")
out=open(output,"w")
out.write(first_line)
for msa in data["order"]:
	for region_size in data[msa]["order"]:
		for depth in data[msa][region_size]["order"]:
			out.write("\n" + msa + "," + region_size + "," + depth)
			for attribute in attributes:
				value_to_be_average=data[msa][region_size][depth][attribute]
				lenght=len(value_to_be_average)
				sum=0
				for value in value_to_be_average:
					sum = sum + value
				mean = sum/lenght
				sum_var = 0
				for value in value_to_be_average:
					sum_var += (value-mean)**2
				var = sum_var / len(value_to_be_average)
				st_dev = math.sqrt(var)
				out.write(",{:.1f}".format(mean)+",{:.1f}".format(st_dev))
