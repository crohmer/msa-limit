#!/usr/bin/env python3
from Bio import SearchIO
import sys,re,os
from collections import Counter
import tool_consensus

"""
		Project "The Limits of Multiple Aligners"

Authors: ROHMER Coralie
Date: 2020-02

Description:

"""
#Script use
def use():
	print("\nScript:\tdata_formating.py",
          "Objectif: \n",
          "To Run:",
		  "./sequence_consensus.py -in <file1> <file2>... -t <file>\n",
          "Arguments: ",
		  "  -required: -in : file alignement (exonerate).",
		  "             -t  : output time_format.pl",sep="\n")
	sys.exit()


#Argument testing
try:
    files_alignement=[sys.argv[sys.argv.index("-in")+1]]
except:
	print("ERROR: The name of the input exonerate file(s) is missing.\n")
	use()
else:
	i=2
	try:
		next_file = sys.argv[sys.argv.index("-in")+i]
	except:
		end_files = 1
	else:
		end_files = 0

	while ( end_files == 0 and re.search("-", next_file) == None ):
		files_alignement.append(next_file)
		i+=1
		try:
			next_file = sys.argv[sys.argv.index("-in")+i]
		except:
			end_files = 1

TIME=True
try:
    file_time=[sys.argv[sys.argv.index("-t")+1]][0]
except:
	TIME=False

#Main
data = {}
for file in files_alignement:
	all_qresult = list(SearchIO.parse(file, 'exonerate-text'))

	for i in range(len(all_qresult)):
		qresult = all_qresult[i]
		id_query = qresult.id.split("_")
		nb_read = id_query[-1][1:]
		read_size = id_query[-2][1:]
		MSA = id_query[-3]
		hsp = qresult[0][0]
		similarity=hsp.aln_annotation['similarity']
		seq=hsp.query
		occurence_seq = Counter(seq)
		occurence_seq_hit = Counter(hsp.hit)
		occurence_similarity = Counter(similarity)
		nb_ambiguity=0
		nb_identity=0
		nb_deletion=0
		nb_insertion=0

		for i in "RYSWKMBDHVN":
			if (i in occurence_seq):
				nb_ambiguity += occurence_seq[i]

		if ("-" in occurence_seq):
			nb_gap = occurence_seq["-"]
		else :
			nb_gap = 0

		size_seq=len(seq) - nb_gap

		for i in range(0,len(seq)):
			if(seq[i]==hsp.hit[i]):
				nb_identity+=1
			else:
				if(seq[i] == "-" ):
					nb_deletion+=1
				else:
					if(hsp.hit[i]== "-" ):
						nb_insertion+=1
		if (MSA not in data):
			data[MSA]={}

		if (read_size not in data[MSA]):
			data[MSA][read_size] = {}
		match=round(tool_consensus.score_between_two_iupac(hsp.hit.seq,seq.seq),1)
		error=round(len(seq)-nb_identity,1)
		nb_substitution = round(error - nb_deletion - nb_insertion,1)
		r_ambiguity=round( (nb_ambiguity/(size_seq)*100) ,1)
		r_identity=round( (nb_identity/len(seq)*100) ,1)
		r_match=round( (match/len(seq)*100) ,1)
		r_error=round( (error/len(seq)*100) ,1)
		if (error != 0):
			r_sub=round((nb_substitution/error*100),1)
			r_del=round((nb_deletion/error*100),1)
			r_ins=round((nb_insertion/error*100),1)
		else:
			r_sub=0
			r_del=0
			r_ins=0
		data[MSA][read_size][nb_read]=[nb_ambiguity,r_ambiguity,nb_identity,r_identity,match,r_match,
		error,r_error,nb_substitution,r_sub,nb_deletion,r_del,nb_insertion,r_ins,size_seq]

if (TIME == True):
	pass
	file_read = open(file_time, "r")
	for line in file_read.readlines():
		if ( not re.search("^MSA", line) ):
				tab_line = re.split(",",line)
				msa = tab_line[0]
				read_size = tab_line[1]
				nb_read = tab_line[2]
				#user = float(tab_line[3])
				#system = float(tab_line[4])
				time = float(tab_line[5])
				elapsed = float(tab_line[6])
				#cpu = float(tab_line[7])
				memory = float(tab_line[8])

				if msa in data:
					if read_size in data[msa]:
						if nb_read in data[msa][read_size]:
							data[msa][read_size][nb_read] += [time,elapsed,memory]

#output
sep=","
print("MSA","region_size","depth","number_Ambiguity","percentage_Ambiguity","number_Identity","percentage_Identity","number_Match","percentage_Match","number_Error","percentage_Error","number_Substitution","percentage_Substitution","number_Deletion","percentage_Deletion","number_Insertion","percentage_Insertion","size",sep=sep,end="")
if (TIME == True):
	print(sep+"time","elapsed","memory",sep=sep)
else:
	print("")
for MSA in data:
	for read_size in data[MSA]:
		for nb_read in data[MSA][read_size]:
			print(MSA,read_size,nb_read,sep=sep,end="")
			for val in data[MSA][read_size][nb_read]:
				print(sep,val,sep="",end="")
				pass
			print()
