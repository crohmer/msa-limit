#!/bin/bash
source ./src/config_conda.sh
if [[ ! -d .conda_snakemake ]]; then
  echo "Create a conda env for snakemake"
  conda env create -p .conda_snakemake -f env_conda/snakemake.yaml  >/dev/null
fi
CURRENT_PATH=`pwd`
conda activate $CURRENT_PATH/.conda_snakemake

if [[ ! -d doc ]]; then
  mkdir doc
fi

snakemake --rulegraph --configfile configuration_files/test.yaml| dot |display
