#!/bin/bash
MSA_ORDER=$1
INPUT=$2
OUTPUT_OUT=$3
OUTPUT_TIME=$4
LOG=$5
OUTPUT_FLOW=$6
ORDER="/usr/bin/time -o $OUTPUT_TIME $MSA_ORDER"
echo "ORDER: $ORDER" >$LOG
if [ `cat $INPUT|wc -l` -ne 0 ]; then
   ERROR=0
   if [ $OUTPUT_FLOW -ne 0 ]; then
     $ORDER >>$LOG 2>&1 || ERROR=1
   else
     $ORDER 2>>$LOG >$OUTPUT_OUT || ERROR=1
   fi
   if [ $ERROR -ne 0 ]; then
     echo "ERROR: msa software crash" >>$LOG
     touch $OUTPUT_OUT
     echo "" >$OUTPUT_TIME
   fi
else
   echo "ERROR: $INPUT File empty" >>$LOG
   touch $OUTPUT_OUT $OUTPUT_TIME
fi
