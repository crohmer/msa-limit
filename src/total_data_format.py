#!/usr/bin/env python3
import sys,re,os
import subprocess
EXP = "experiments"
ATTRIBUTES_TO_DISPLAY=["percentage_Ambiguity","sd_percentage_Ambiguity","percentage_Identity","sd_percentage_Identity","percentage_Match","sd_percentage_Match","percentage_Error","sd_percentage_Error","percentage_Substitution","sd_percentage_Substitution","percentage_Deletion","sd_percentage_Deletion","percentage_Insertion","sd_percentage_Insertion","size","sd_size"]
ATTRIBUTES_TO_DISPLAY_THRESHOLD_INDEPENDANT=["time","memory","sd_time","sd_memory"]
PREFIX="start_position_"
RESULT_FOLDER="results"
NAME_DATA_FILE="data_align_t"
NAME_META_CONSENSUS="meta_consensus_"
NAME_DIPLOID="diploid_data_align"
#Script use
def use():
	print("\nScript:\ttotal_data_formating.py",
          "Objectif: \n",
          "To Run:",
		  "./total_data_formatting.py -n <exp_name1> <exp_name2> <exp_name3> ...\n",
          "Arguments: ",
		  "  required: ",
		  "		-n <string> <string>... : name of the experiments you want to display.",
		  "  optional: ",
		  "     -o <string> : output folders",
		  "     -m : for meta consensus",
		  "     -d : for diploid metrique",sep="\n")
	sys.exit()
try:
    output_dir= sys.argv[sys.argv.index("-o")+1]
except:
	output_dir="results"

try:
    exp_names= [sys.argv[sys.argv.index("-n")+1]]
except:
	print("ERROR: The name of the experiments are missing.\n")
	use()
else:
	i=2
	try:
		next_name = sys.argv[sys.argv.index("-n")+i]
	except:
		end_names = 1
	else:
		end_names = 0

	while ( end_names == 0 and re.search("-", next_name) == None ):
		exp_names.append(next_name)
		i+=1
		try:
			next_name = sys.argv[sys.argv.index("-n")+i]
		except:
			end_names = 1

META_CONSENSUS=True
DIPLOID=False
try:
    a = sys.argv[sys.argv.index("-m")]
except:
	META_CONSENSUS=False
	try:
		a = sys.argv[sys.argv.index("-d")]
		DIPLOID=True

		ATTRIBUTES_TO_DISPLAY=["nb_iupac_ref","nb_iupac_seq_consensus","identical_iupac",
		"iupac_vs_iupac","sc_iupac_vs_r_nuc","sc_iupac_vs_r_gap","sc_nuc_vs_r_iupac",
		"sc_gap_vs_r_iupac",
		"sd_nb_iupac_ref","sd_nb_iupac_seq_consensus","sd_identical_iupac","sd_iupac_vs_iupac",
		"sd_sc_iupac_vs_r_nuc","sd_sc_iupac_vs_r_gap","sd_sc_nuc_vs_r_iupac","sd_sc_gap_vs_r_iupac"]
		ATTRIBUTES_TO_DISPLAY_THRESHOLD_INDEPENDANT=[]
	except:
		pass

files={}
i=0
result = subprocess.run("if [ ! -d "+output_dir+"_mean ]; then mkdir "+output_dir+"_mean;fi",shell=True)
result = subprocess.run("if [ ! -d "+output_dir+"_all_start_positions ]; then mkdir "+output_dir+"_all_start_positions;fi",shell=True)
#-----------------------------------------------------------------------------
# Retrieved what was needed to read the files
#-----------------------------------------------------------------------------
threshold=0
for i in range(len(exp_names)):
	if os.path.exists(os.path.join(EXP,exp_names[i])):
		dirs=os.listdir(os.path.join(EXP,exp_names[i]))
		dirs.append("")
		if(RESULT_FOLDER in dirs):
			for dir in dirs:
				if re.search(PREFIX,dir) or dir == "":
					for root, dirs, current_files in os.walk(os.path.join(EXP,exp_names[i],dir,RESULT_FOLDER)):
						for filename in current_files:
							if re.search(NAME_DATA_FILE,filename):
								search_meta_consensus=re.search(NAME_META_CONSENSUS,filename)
								search_diploid=re.search(NAME_DIPLOID,filename)
								if ((search_meta_consensus and META_CONSENSUS)
									or (not search_meta_consensus and not META_CONSENSUS and not DIPLOID and not search_diploid)
									or(search_diploid and DIPLOID)):
									threshold=filename.split(".")[0].split("_t")[-1]
									if (threshold not in files):
										files[threshold]={}
									if (exp_names[i] not in files[threshold]):
										files[threshold][exp_names[i]]={}
									files[threshold][exp_names[i]][dir]=filename
		else:
			print("ERROR: Incomplete data for the experiment " + exp_names[i])
	else:
		print("ERROR: The name " + exp_names[i] + " isn't the name of an experiment")
last_threshold=threshold
#-----------------------------------------------------------------------------
# Recovered data from files
#-----------------------------------------------------------------------------
data={}
bases=[]
reads=[]
MSA={}
for threshold in files :
	data[threshold]={}
	data[threshold]["order"]=[]
	for exp_name in files[threshold] :
		data[threshold][exp_name]={}
		data[threshold][exp_name]["order"]=[]
		data[threshold]["order"]=data[threshold]["order"] + [exp_name]
		header=[]
		header_size=0
		for dir in files[threshold][exp_name]:
			data[threshold][exp_name][dir] = {}
			MSA[exp_name]=[]
			data[threshold][exp_name]["order"]=data[threshold][exp_name]["order"] + [dir]
			name_file = os.path.join(EXP,exp_name,dir,RESULT_FOLDER,files[threshold][exp_name][dir])
			try:
				read = open(name_file, "r")
				for line in read.readlines():
					line=line.strip('\n')
					tab_line=line.split(",")
					if not (tab_line[0]=="MSA") :
						msa=tab_line[0]
						base_pair=int(tab_line[1])
						nb_read=int(tab_line[2])

						if (base_pair not in data[threshold][exp_name][dir]):
							data[threshold][exp_name][dir][base_pair] = {}
						if (nb_read not in data[threshold][exp_name][dir][base_pair]):
							data[threshold][exp_name][dir][base_pair][nb_read] = {}

						if (base_pair not in bases):
							bases.append(base_pair)
						if (nb_read not in reads):
							reads.append(nb_read)
						if (msa not in MSA[exp_name]):
							MSA[exp_name].append(msa)

						data[threshold][exp_name][dir][base_pair][nb_read][msa] = {}
						for i in range(3,header_size):
							data[threshold][exp_name][dir][base_pair][nb_read][msa][header[i]] = tab_line[i]
					else:
						header=tab_line
						header_size=len(header)
				read.close()
				MSA[exp_name]=sorted(MSA[exp_name])
			except:
				print(name_file , "don't exists.")
#-----------------------------------------------------------------------------
# Write to output files
#-----------------------------------------------------------------------------
for threshold in files :
	add_name_file_output=""
	attributes_to_display=ATTRIBUTES_TO_DISPLAY
	if (not META_CONSENSUS):
		if (threshold==last_threshold):
			attributes_to_display+=ATTRIBUTES_TO_DISPLAY_THRESHOLD_INDEPENDANT
	else:
		add_name_file_output=NAME_META_CONSENSUS
	#print(attributes_to_display)
	for attribute in attributes_to_display:
		#Header for files
		if attribute in ATTRIBUTES_TO_DISPLAY_THRESHOLD_INDEPENDANT:
			pass
			output_mean=open(output_dir+"_mean/data_" + add_name_file_output + "mean_"+ attribute + ".csv","w")
			if(not re.search("^sd_",attribute)):
				output_all=open(output_dir+"_all_start_positions/data_" + add_name_file_output + "all_start_position_"+ attribute + ".csv","w")
		else:
			output_mean=open(output_dir+"_mean/data_" + add_name_file_output + "mean_"+ attribute + "_" + threshold + ".csv","w")
			if(not re.search("^sd_",attribute)):
				output_all=open(output_dir+"_all_start_positions/data_" + add_name_file_output + "all_start_position_"+ attribute + "_" + threshold + ".csv","w")
		output_mean.write(",,")
		if(not re.search("^sd_",attribute)):
			output_all.write(",,")
		for exp_name in data[threshold]["order"]:
			for dir in data[threshold][exp_name]["order"]:
				if dir == "":
					output_mean.write("," + exp_name)
					for msa in MSA[exp_name]:
						output_mean.write(",")
				else:
					if(not re.search("^sd_",attribute)):
						output_all.write("," + os.path.join(exp_name,dir))
						for msa in MSA[exp_name]:
							output_all.write(",")
		output_mean.write("\nlenght,cover,")
		if(not re.search("^sd_",attribute)):
			output_all.write("\nlenght,cover,")
		for exp_name in data[threshold]["order"]:
			for dir in data[threshold][exp_name]["order"]:
				if dir == "":
					for msa in MSA[exp_name]:
						output_mean.write("," + msa)
					output_mean.write(",")
				else:
					if(not re.search("^sd_",attribute)):
						for msa in MSA[exp_name]:
							output_all.write("," + msa)
						output_all.write(",")
		output_mean.write("\n")
		if(not re.search("^sd_",attribute)):
			output_all.write("\n")

		# Writing data
		bases=sorted(bases)
		reads=sorted(reads)
		for base in bases :
			for read in reads :
				output_mean.write(str(base) + "," + str(read) + ",")
				if(not re.search("^sd_",attribute)):
					output_all.write(str(base) + "," + str(read) + ",")
				for exp_name in data[threshold]["order"]:
					for dir in data[threshold][exp_name]["order"]:
						if dir == "":
							for msa in MSA[exp_name]:
								if (base in data[threshold][exp_name][dir]) and (read in data[threshold][exp_name][dir][base]) and (msa in data[threshold][exp_name][dir][base][read]):
									output_mean.write("," + data[threshold][exp_name][dir][base][read][msa][attribute])
								else:
									output_mean.write(",")
							output_mean.write(",")
						else:
							if(not re.search("^sd_",attribute)):
								for msa in MSA[exp_name]:
									if (base in data[threshold][exp_name][dir]) and (read in data[threshold][exp_name][dir][base]) and (msa in data[threshold][exp_name][dir][base][read]):
										output_all.write("," + data[threshold][exp_name][dir][base][read][msa][attribute])
									else:
										output_all.write(",")
								output_all.write(",")
				output_mean.write("\n")
				if(not re.search("^sd_",attribute)):
					output_all.write("\n")
			output_mean.write("\n")
			if(not re.search("^sd_",attribute)):
				output_all.write("\n")
