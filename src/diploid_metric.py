#!/usr/bin/env python3
from Bio import SearchIO
import sys,re,os
from collections import Counter
import tool_consensus as tc

"""
		Project "The Limits of Multiple Aligners"

Authors: ROHMER Coralie
Date: 2020-02

Description:

"""
#Script use
def use():
	print("\nScript:\tdata_formating.py",
          "Objectif: \n",
          "To Run:",
	  "   ./diploid_metric.py -in <file1> <file2>... -o <output_file>",
          "Arguments: ",
	  "   -required: -in : file alignement (exonerate)",
	  "              -o  : name of the output file",sep="\n")
	sys.exit()

#Argument testing
try:
    files_alignement=[sys.argv[sys.argv.index("-in")+1]]
except:
	print("ERROR: The name of the input exonerate file(s) is missing.\n")
	use()
else:
	i=2
	try:
		next_file = sys.argv[sys.argv.index("-in")+i]
	except:
		end_files = 1
	else:
		end_files = 0

	while ( end_files == 0 and re.search("-", next_file) == None ):
		files_alignement.append(next_file)
		i+=1
		try:
			next_file = sys.argv[sys.argv.index("-in")+i]
		except:
			end_files = 1
try:
    output=sys.argv[sys.argv.index("-o")+1]
except:
	print("ERROR: The name of the output file is missing.\n")
	use()


#Main
data = {}
read_sizes=[]
depths=[]
for file in files_alignement:
	all_qresult = list(SearchIO.parse(file, 'exonerate-text'))

	for i in range(len(all_qresult)):
		qresult = all_qresult[i]
		id_query = qresult.id.split("_")
		nb_read = int(id_query[-1][1:])
		read_size = int(id_query[-2][1:])
		MSA = id_query[-3]
		hsp = qresult[0][0]
		similarity=hsp.aln_annotation['similarity']
		seq=hsp.query
		occurence_seq = Counter(seq)
		occurence_seq_hit = Counter(hsp.hit)
		occurence_similarity = Counter(similarity)
		nb_ambiguity_seq_consensus=0
		nb_ambiguity_ref=0
		for i in "RYSWKMBDHVN":
			if (i in occurence_seq):
				nb_ambiguity_seq_consensus += occurence_seq[i]

		for i in "RYSWKMBDHVN":
			if (i in occurence_seq_hit):
				nb_ambiguity_ref += occurence_seq_hit[i]

		identical_iupac=0
		iupac_vs_iupac=0
		iupac_vs_nuc=0
		iupac_vs_nucIupac=0
		iupac_vs_nucNoIupac=0
		iupac_vs_gap=0
		nuc_vs_iupac=0
		nucIupac_vs_iupac=0
		nucNoIupac_vs_iupac=0
		gap_vs_iupac=0
		for i in range(0,len(seq)):
			if(seq[i] in "RYSWKMBDHVN"):
				if( hsp.hit[i] in "RYSWKMBDHVN"):
					if(seq[i]==hsp.hit[i]):
						identical_iupac+=1
					else:
						iupac_vs_iupac+=1
				else:
					if(hsp.hit[i]=="-"):
						iupac_vs_gap+=1
					else:
						iupac_vs_nuc+=1
						if(tc.iupac_to_nuc[seq[i]].find(hsp.hit[i]) == -1):
							iupac_vs_nucNoIupac+=1
						else:
							iupac_vs_nucIupac+=1
			else:
				if(hsp.hit[i] in "RYSWKMBDHVN"):
					if(seq[i]=="-"):
						gap_vs_iupac+=1
					else:
						nuc_vs_iupac+=1
						if(tc.iupac_to_nuc[hsp.hit[i]].find(seq[i]) == -1):
							nucNoIupac_vs_iupac+=1
						else:
							nucIupac_vs_iupac+=1

		if (MSA not in data):
			data[MSA]={}

		if (read_size not in data[MSA]):
			data[MSA][read_size] = {}

		if (read_size not in read_sizes):
			read_sizes.append(read_size)

		if (nb_read not in depths):
			depths.append(nb_read)

		data[MSA][read_size][nb_read]=[nb_ambiguity_ref,nb_ambiguity_seq_consensus,identical_iupac,iupac_vs_iupac,iupac_vs_nuc,iupac_vs_nucNoIupac,iupac_vs_nucIupac,iupac_vs_gap,nuc_vs_iupac,nucNoIupac_vs_iupac,nucIupac_vs_iupac,gap_vs_iupac]
depths.sort()
read_sizes.sort()
#output
sep=","
output=open(output,"w")
output.write("MSA,region_size,depth,nb_iupac_ref,nb_iupac_seq_consensus,identical_iupac,iupac_vs_iupac,sc_iupac_vs_r_nuc,sc_iupac_vs_r_nucNoIupac,sc_iupac_vs_r_nucIupac,sc_iupac_vs_r_gap,sc_nuc_vs_r_iupac,sc_nucNoIupac_vs_r_iupac,sc_nucIupac_vs_r_iupac,sc_gap_vs_r_iupac\n")
for MSA in data:
	for read_size in read_sizes:
		if(read_size in data[MSA]):
			for nb_read in depths:
				if(nb_read in data[MSA][read_size]):
					output.write(MSA  + "," + str(read_size) + "," + str(nb_read))
					for val in data[MSA][read_size][nb_read]:
						output.write("," + str(val))
						pass
					output.write("\n")
