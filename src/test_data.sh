#!/bin/bash
source ./src/config_conda.sh

if [[ ! -d .conda_pbsim2 ]]; then
  echo "Create a conda env for pbsim2"
  conda env create -p .conda_pbsim2 -f env_conda/pbsim2.yaml  >/dev/null
fi
CURRENT_PATH=`pwd`
conda activate $CURRENT_PATH/.conda_pbsim2
if [ ! -d "test_data" ];then
mkdir "test_data"
fi

cd test_data

echo "Download reference..."
wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/840/245/GCF_000840245.1_ViralProj14204/GCF_000840245.1_ViralProj14204_genomic.fna.gz >/dev/null
gunzip GCF_000840245.1_ViralProj14204_genomic.fna.gz >/dev/null
echo "Download model..."
wget https://github.com/yukiteruono/pbsim2/raw/master/data/R103.model >/dev/null
REF="GCF_000840245.1_ViralProj14204_genomic.fna"
SIZE=10000
DEPTH=100
MODEL=R103.model

echo "Simulation of reads (pbsim2)"
pbsim $REF --seed 123456 --hmm_model $MODEL --depth $DEPTH --difference-ratio 23:31:46 --accuracy-mean 0.90 --prefix simulated_read_lambda >/dev/null

rm *.maf *.ref

PATH_DATA=`pwd`
cd ..

echo "Creating configuration files..."
if [ ! -d "configuration_files" ];then
  mkdir "configuration_files"
fi
NAME="test"
IN="$PATH_DATA/simulated_read_lambda_0001.fastq"
REF="$PATH_DATA/GCF_000840245.1_ViralProj14204_genomic.fna"
echo -e "N: $NAME\nI: $IN\nR: $REF" >configuration_files/$NAME.yaml
