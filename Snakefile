from datetime import datetime
import os
configfile: "config.yaml"

EXP = "experiments"
PREFIX_START_POSITION="start_position_"
#Script usage
def usage():
    print("\nScript:\tsnakemake",
        "Objectif: \n",
        "To Run:",
        'snakemake -p --config in="<file_reads>" r="<file_ref>" b="<int>,<int2>..."\n',
        "Arguments: ",
        "  -required: i : nanoport long reads",
        "             r : reference sequences. (1 or 2)",
        "  -optional: o : number of start positions to be tested",
        "             b : start position(s) (replacing -o).",
        "             d : depth(s).",
        "             s : sizes of regions",
        "             t : threshold for sequence consensus",
        "             m : MSA to run (muscle,mafft,poa,kalign,spoa,kalign3,clustalo).",
        "             n : exp name",
        '\nEx: snakemake --config in="covid.fastq" r="ref_covid.fasta" b="1,15000" name="covid" d="10,100" s="100,200" t="50" m="mafft".',sep="\n")
    sys.exit()

#Argument testing
try:
    READ_FILE = os.path.abspath(config['I'])
except:
    print("ERROR: The name of the input nanoport long reads file is missing.\n")
    usage()

try:
    REF_FILE = os.path.abspath(config['R'])
    ref = open(REF_FILE, "r")
    genome_seq = ""
    genome_size_min = 0
    for line in ref.readlines():
        if (line[0] == ">"):
            genome_size = len(genome_seq)
            if(genome_size_min == 0 or genome_size_min > genome_size):
                genome_size_min = genome_size
            genome_seq=""
        else:
            genome_seq = genome_seq + line
    genome_size = len(genome_seq)
    if(genome_size_min == 0 or genome_size_min > genome_size):
        genome_size_min = genome_size
except:
    print("ERROR: The input references sequences file is missing.\n")
    usage()


REGION_SIZES = config['S']

DEPTHS = config['D']

THRESHOLDS = config['T']

MSA = config['M']

try:
    EXP_NAME = config['N']
except:
    now = datetime.now()
    dt_string = now.strftime("%d%m%Y%H%M%S")
    EXP_NAME = dt_string

try:
    beginings = config['B']
except:
    try:
        nb_start_position = int(config['O'])
    except:
        nb_start_position = 1
    beginings=[]
    first_begining = (genome_size_min - int(REGION_SIZES[-1]))/(nb_start_position+1)
    for i in range(nb_start_position):
        beginings.append(int((i+1)*first_begining))

DATA_SETS=[]
i=0

if type(beginings) != int: # equal : if len(beginings) > 1
   for start in beginings:
       i += 1
       DATA_SETS.append(os.path.join(EXP,EXP_NAME,PREFIX_START_POSITION + str(start)))
else:
    DATA_SETS.append(os.path.join(EXP,EXP_NAME,PREFIX_START_POSITION + str(beginings)))
    beginings = [int(beginings)]



ATTRIBUTES_DATA = ["number_Ambiguity","percentage_Ambiguity","number_Identity","percentage_Identity","number_Error","percentage_Error","number_Match","percentage_Match","size","time","elapsed","memory"]

onstart:
    print("Workflow Start")

onsuccess:
    os.system('grep -r "ERROR" ' + EXP + '/'+EXP_NAME + '/*/logs ' + EXP + '/'+EXP_NAME + '/logs  >' +EXP + '/'+ EXP_NAME + '/final_log')
    os.system('if [ `ls|grep ".dnd"|wc -l` -ne 0 ]; then rm *.dnd;fi')
    print("Workflow finished, no error")

onerror:
    os.system('grep -r "ERROR" ' + EXP + '/'+EXP_NAME + '/*/logs ' + EXP + '/'+EXP_NAME + '/logs  >' + EXP + '/'+EXP_NAME + '/final_log')
    os.system('if [ `ls|grep ".dnd"|wc -l` -ne 0 ]; then rm *.dnd;fi')
    print("An error has occurred. Please look in log files: " + EXP + '/'+EXP_NAME + "/final_log")

#-------------------------------------------------------------------------------
# Start Workflow
#-------------------------------------------------------------------------------
rule all :
    input :
        expand(EXP + "/" + EXP_NAME + "/results/" + EXP_NAME + "_data_align_t{threshold}.csv", data_set=DATA_SETS, threshold=THRESHOLDS),
        expand('{data_set}/results/' + EXP_NAME + '_graph_{attribute}.pdf', data_set=DATA_SETS, attribute=ATTRIBUTES_DATA),
        expand(EXP + '/'+EXP_NAME + '/results/' + EXP_NAME + '_graph_{attribute}.pdf', data_set=DATA_SETS, attribute=ATTRIBUTES_DATA),
        expand(EXP + "/" + EXP_NAME + "/results/" + EXP_NAME + "_meta_consensus_data_align_t{threshold}.csv", threshold=THRESHOLDS),
        os.path.join(EXP,EXP_NAME,'results','dataset_stats.txt')

#-------------------------------------------------------------------------------
# Data set preparation
#-------------------------------------------------------------------------------
rule data_set_preparation :
    input :
        READ_FILE,
        REF_FILE
    output :
        os.path.join(EXP,EXP_NAME,'data','reads.fasta'),
        os.path.join(EXP,EXP_NAME,'data','ref.fasta'),
        expand(os.path.join('{data_set}','start_position.txt') , data_set = DATA_SETS)
    message :
        "Data set preparation for "+EXP + '/'+ EXP_NAME
    log:
        os.path.join(EXP,EXP_NAME,'logs','1_data_set_preparation.log')
    conda:
        "env_conda/python3.yaml"
    shell:
        'ORDER="./src/data_set_preparation.py -i {input} -p '+PREFIX_START_POSITION+' -n '+ EXP + '/'+EXP_NAME + ' -b ' + ' '.join(map(str,beginings)) + '";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER 2>&1 >>{log}'

#-------------------------------------------------------------------------------
# Alignment on the reference genome with Minimap2
#-------------------------------------------------------------------------------
rule alignment_reads_on_ref :
    input :
        os.path.join(EXP,EXP_NAME,'data','ref.fasta') ,
        os.path.join(EXP,EXP_NAME,'data','reads.fasta')
    output :
        os.path.join(EXP,EXP_NAME,'alignement','aln_reads_on_ref.sam')
    message :
        "Minimap for "+EXP + '/'+ EXP_NAME
    log:
        os.path.join(EXP,EXP_NAME,'logs','2_alignment_reads_on_ref.log')
    conda:
        "env_conda/minimap2.yaml"
    shell :
        'ORDER="minimap2 -cax map-ont -t 8  {input}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER  >{output} 2>> {log}'

#-------------------------------------------------------------------------------
# Selection of reads
#-------------------------------------------------------------------------------
rule reads_map_region :
    input :
        aln = os.path.join(EXP,EXP_NAME,'alignement','aln_reads_on_ref.sam'),
        start = os.path.join('{data_set}','start_position.txt')
    output :
        os.path.join('{data_set}','read_map_region','reads_r{region_size}.fasta')
    message:
        "Reads map region for {wildcards.data_set} (Region size={wildcards.region_size})"
    log:
        os.path.join('{data_set}','logs','3_reads_map_region_r{region_size}.log')
    conda:
        "env_conda/perl.yaml"
    shell :
        'ORDER="./src/reads_map_region.pl -s `cat {input.start}` -t {wildcards.region_size} {input.aln} {output}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER  2>&1 >>{log}'

rule shuffle_fasta :
    input :
        os.path.join('{data_set}','read_map_region','reads_r{region_size}.fasta')
    output :
        os.path.join('{data_set}','read_map_region','read_shuffle_r{region_size}.fasta')
    message:
        "shuffle fasta for {wildcards.data_set} (Region size={wildcards.region_size})"
    log:
        os.path.join('{data_set}','logs','4_shuffle_fasta_r{region_size}.log')
    conda:
        "env_conda/perl.yaml"
    shell :
        'ORDER="./src/shuffle_fasta.pl {input}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER  >{output} 2>>{log}'

rule select_depth :
    input :
        os.path.join('{data_set}','read_map_region','read_shuffle_r{region_size}.fasta')
    output :
        os.path.join('{data_set}','selected_read','reads_r{region_size}_d{depth}.fasta')
    message:
        "Select depth for {wildcards.data_set} (Region size={wildcards.region_size} & Depth={wildcards.depth})"
    log:
        os.path.join('{data_set}','logs','5_select_number_reads_r{region_size}_d{depth}.log')
    shell :
        'ORDER="./src/selected_nb_read.sh {input} {wildcards.depth} {wildcards.data_set}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER >{output} 2>>{log}'

#-------------------------------------------------------------------------------
# Launch of tested MSA tools
#-------------------------------------------------------------------------------
rule muscle :
    input :
        os.path.join('{data_set}','selected_read','reads_r{region_size}_d{depth}.fasta')
    output :
        time = os.path.join('{data_set}','time','MSA_muscle_r{region_size}_d{depth}'),
        out = os.path.join('{data_set}','msa','MSA_muscle_r{region_size}_d{depth}.fasta')
    message:
        "Muscle for {wildcards.data_set} (Region size={wildcards.region_size} & Depth={wildcards.depth})"
    log:
        os.path.join('{data_set}','logs','6_muscle_r{region_size}_d{depth}.log')
    conda:
        "env_conda/muscle.yaml"
    shell :
        './src/run_MSA.sh "muscle -in {input} -out {output.out}" {input} {output.out} {output.time} {log} 1'

rule mafft :
    input :
        os.path.join('{data_set}','selected_read','reads_r{region_size}_d{depth}.fasta')
    output :
        time = os.path.join('{data_set}','time','MSA_mafft_r{region_size}_d{depth}'),
        out = os.path.join('{data_set}','msa','mafft_uncorrected_r{region_size}_d{depth}.fasta')
    message:
        "Mafft for {wildcards.data_set} (Region size={wildcards.region_size} & Depth={wildcards.depth})"
    log:
        os.path.join('{data_set}','logs','6_mafft_r{region_size}_d{depth}.log')
    conda:
        "env_conda/mafft.yaml"
    shell :
        './src/run_MSA.sh "mafft {input}" {input} {output.out} {output.time} {log} 0'

rule mafft_correction :
    input :
        os.path.join('{data_set}','msa','mafft_uncorrected_r{region_size}_d{depth}.fasta')
    output :
        os.path.join('{data_set}','msa','MSA_mafft_r{region_size}_d{depth}.fasta')
    message:
        "Mafft correction for {wildcards.data_set} (Region size={wildcards.region_size} & Depth={wildcards.depth})"
    shell :
        'cat {input} | tr "atgc" "ATGC" >{output};'

rule clustalo :
    input :
        os.path.join('{data_set}','selected_read','reads_r{region_size}_d{depth}.fasta')
    output :
        time = os.path.join('{data_set}','time','MSA_clustalo_r{region_size}_d{depth}'),
        out = os.path.join('{data_set}','msa','MSA_clustalo_r{region_size}_d{depth}.fasta')
    message:
        "Clustalo for {wildcards.data_set} (Region size={wildcards.region_size} & Depth={wildcards.depth})"
    log:
        os.path.join('{data_set}','logs','6_clustalo_r{region_size}_d{depth}.log')
    conda:
        "env_conda/clustalo.yaml"
    shell :
        './src/run_MSA.sh "clustalo --force --in {input} --out {output.out}" {input} {output.out} {output.time} {log} 1'

rule tcoffee :
    input :
        os.path.join('{data_set}','selected_read','reads_r{region_size}_d{depth}.fasta')
    output :
        time = os.path.join('{data_set}','time','MSA_tcoffee_r{region_size}_d{depth}'),
        out = os.path.join('{data_set}','msa','MSA_tcoffee_r{region_size}_d{depth}.fasta')
    message:
        "T Coffee for {wildcards.data_set} (Region size={wildcards.region_size} & Depth={wildcards.depth})"
    log:
        os.path.join('{data_set}','logs','6_tcoffee_r{region_size}_d{depth}.log')
    conda:
        "env_conda/t_coffee.yaml"
    shell :
        './src/run_MSA.sh "t_coffee -in={input} -outfile={output.out}" {input} {output.out} {output.time} {log} 1'

rule poa :
    input :
        os.path.join('{data_set}','selected_read','reads_r{region_size}_d{depth}.fasta')
    output :
        time = os.path.join('{data_set}','time','MSA_poa_r{region_size}_d{depth}'),
        out = os.path.join('{data_set}','msa','MSA_poa_r{region_size}_d{depth}.fasta')
    message:
        "Poa for {wildcards.data_set} (Region size={wildcards.region_size} & Depth={wildcards.depth})"
    log:
        os.path.join('{data_set}','logs','6_poa_r{region_size}_d{depth}.log')
    conda:
        "env_conda/poa.yaml"
    shell :
        './src/run_MSA.sh "poa -read_fasta {input} -clustal {output.out} poa_matrice/blosum80.mat" {input} {output.out} {output.time} {log} 1'

rule spoa :
    input :
        os.path.join('{data_set}','selected_read','reads_r{region_size}_d{depth}.fasta')
    output :
        time = os.path.join('{data_set}','time','MSA_spoa_r{region_size}_d{depth}'),
        out = os.path.join('{data_set}','msa','MSA_spoa_r{region_size}_d{depth}.fasta')
    message:
        "Spoa for {wildcards.data_set} (Region size={wildcards.region_size} & Depth={wildcards.depth})"
    log:
        os.path.join('{data_set}','logs','6_spoa_r{region_size}_d{depth}.log')
    conda:
        "env_conda/spoa.yaml"
    shell :
        './src/run_MSA.sh "spoa -r 1 -l 1 {input}" {input} {output.out} {output.time} {log} 0'

rule kalign :
    input :
        os.path.join('{data_set}','selected_read','reads_r{region_size}_d{depth}.fasta')
    output :
        time = os.path.join('{data_set}','time','MSA_kalign_r{region_size}_d{depth}'),
        out = os.path.join('{data_set}','msa','MSA_kalign_r{region_size}_d{depth}.fasta')
    message:
        "Kalign for {wildcards.data_set} (Region size={wildcards.region_size} & Depth={wildcards.depth})"
    log:
        os.path.join('{data_set}','logs','6_kalign_r{region_size}_d{depth}.log')
    conda:
        "env_conda/kalign2.yaml"
    shell :
        './src/run_MSA.sh "kalign -i {input} -out {output.out}" {input} {output.out} {output.time} {log} 1'

rule kalign3 :
    input :
        os.path.join('{data_set}','selected_read','reads_r{region_size}_d{depth}.fasta')
    output :
        time = os.path.join('{data_set}','time','MSA_kalign3_r{region_size}_d{depth}'),
        out = os.path.join('{data_set}','msa','MSA_kalign3_r{region_size}_d{depth}.fasta')
    message:
        "Kalign3 for {wildcards.data_set} (Region size={wildcards.region_size} & Depth={wildcards.depth})"
    log:
        os.path.join('{data_set}','logs','6_kalign3_r{region_size}_d{depth}.log')
    conda:
        "env_conda/kalign3.yaml"
    shell :
        './src/run_MSA.sh "kalign -i {input} -o {output.out}" {input} {output.out} {output.time} {log} 1'

rule abpoa :
    input :
        os.path.join('{data_set}','selected_read','reads_r{region_size}_d{depth}.fasta')
    output :
        time = os.path.join('{data_set}','time','MSA_abpoa_r{region_size}_d{depth}'),
        out = os.path.join('{data_set}','msa','abpoa_uncorrected_r{region_size}_d{depth}.fasta')
    message:
        "Abpoa for {wildcards.data_set} (Region size={wildcards.region_size} & Depth={wildcards.depth})"
    log:
        os.path.join('{data_set}','logs','6_abpoa_r{region_size}_d{depth}.log')
    conda:
        "env_conda/abpoa.yaml"
    shell :
        './src/run_MSA.sh "abpoa -r 1 {input}" {input} {output.out} {output.time} {log} 0'

rule abpoa_correction :
    input :
        os.path.join('{data_set}','msa','abpoa_uncorrected_r{region_size}_d{depth}.fasta')
    output :
        os.path.join('{data_set}','msa','MSA_abpoa_r{region_size}_d{depth}.fasta')
    message:
        "Abpoa correction for {wildcards.data_set} (Region size={wildcards.region_size} & Depth={wildcards.depth})"
    shell :
        'tail -{wildcards.depth} {input}| sed "s/^/>seq\\n/" >{output};'
#-------------------------------------------------------------------------------
# Construction of a consensus sequence for each MSA
#-------------------------------------------------------------------------------
rule region_seq :
    input :
        ref = EXP + '/'+ EXP_NAME + '/data/ref.fasta',
        start = "{data_set}/start_position.txt"
    output :
        "{data_set}/seq_selectes_region/region_seq_r{region_size}.fasta"
    message:
        "Region's seq for {wildcards.data_set} (Region size={wildcards.region_size})"
    log:
        "{data_set}/logs/6_region_seq_r{region_size}.log"
    shell :
        'ORDER="./src/region_seq.sh {input.start} {input.ref} {output} {wildcards.region_size}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER 2>&1 >>{log}'

rule sequence_consensus :
    input :
        expand("{{data_set}}/msa/MSA_{msa}_r{{region_size}}_d{depth}.fasta" , msa = MSA, depth=DEPTHS)
    output :
        out="{data_set}/seq_consensus/seq_consensus_t{threshold}_r{region_size}.fasta",
        gap="{data_set}/seq_consensus/seq_consensus_t{threshold}_r{region_size}_with_gap.fasta"
    message:
        "Sequence_consensus for {wildcards.data_set} (Threshold={wildcards.threshold} & Region size={wildcards.region_size})"
    log:
        "{data_set}/logs/7_sequence_consensus_t{threshold}_r{region_size}.log"
    conda:
        "env_conda/python3.yaml"
    shell :
        'ORDER="./src/sequence_consensus.py -I -in {input} -t {wildcards.threshold} -o {output.out} -g {output.gap}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER >{output} 2>>{log}'

#-------------------------------------------------------------------------------
# Alignement of the consensus sequence on the reference region with exonerate
#-------------------------------------------------------------------------------
rule alignment_consensus_ref :
    input :
        consensus="{data_set}/seq_consensus/seq_consensus_t{threshold}_r{region_size}.fasta",
        region="{data_set}/seq_selectes_region/region_seq_r{region_size}.fasta"
    output :
        "{data_set}/seq_consensus/align_consensus_ref_t{threshold}_r{region_size}.txt",
    message:
        "Alignment_consensus_ref for {wildcards.data_set} (Threshold={wildcards.threshold} & Region size={wildcards.region_size})"
    log:
        "{data_set}/logs/8_alignment_consensus_ref_t{threshold}_r{region_size}.log"
    conda:
        "env_conda/exonerate.yaml"
    shell :
        'if [ `cat {input.consensus}|grep ">"|wc -l` -ne 0 ]; then'
        '   ORDER="exonerate --bestn 1 -Q dna -E -m a:g {input.consensus} {input.region}";'
        '   echo "ORDER: $ORDER" >{log};'
        '   $ORDER >{output} 2>>{log};'
        'else'
        '   echo "ERROR: No sequences" >>{log};'
        '   touch {output};'
        'fi'

#-------------------------------------------------------------------------------
# Evaluation of the consensus sequences
#-------------------------------------------------------------------------------
rule data_formatting :
    input :
        exo = expand("{{data_set}}/seq_consensus/align_consensus_ref_t{{threshold}}_r{region_size}.txt" , region_size=REGION_SIZES),
        time = "{data_set}/time/all_MSA_time.csv"
    output :
        "{data_set}/results/"+EXP_NAME+"_data_align_t{threshold}.csv"
    message:
        "Data formatting for {wildcards.data_set} (Threshold={wildcards.threshold})"
    log:
        "{data_set}/logs/9_data_formatting_t{threshold}.log"
    conda:
        "env_conda/python3.yaml"
    shell :
        'ORDER="./src/data_formatting.py -in {input.exo} -t {input.time}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER >{output} 2>>{log}'

rule region_mean:
    input :
        expand("{data_set}/results/"+EXP_NAME+"_data_align_t{{threshold}}.csv" , data_set = DATA_SETS)
    output :
        EXP + '/'+ EXP_NAME + "/results/"+EXP_NAME+"_data_align_t{threshold}.csv"
    message:
        "Region mean for " + EXP + '/'+ EXP_NAME + " (threshold={wildcards.threshold})"
    log:
        EXP + '/'+EXP_NAME + "/logs/4_region_mean_t{threshold}.log"
    conda:
        "env_conda/python3.yaml"
    shell :
        'ORDER="./src/region_mean.py -in {input} -out {output} -t {wildcards.threshold}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER 2>>{log}'

rule data_display :
    input :
        expand("{{data_set}}/results/"+EXP_NAME+"_data_align_t{threshold}.csv", threshold=THRESHOLDS)
    output :
        "{data_set}/results/"+EXP_NAME+"_graph_{attribute}.pdf",
    message:
        "Data display for {wildcards.data_set} (Attribute={wildcards.attribute})"
    log:
        "{data_set}/logs/10_data_display_{attribute}.log"
    conda:
        "env_conda/python3.yaml"
    shell :
        'ORDER="./src/graphical_seq_consensus_analysis.py {wildcards.attribute} {output} {input}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER >{output} 2>>{log}'

# -------------------------------------------------------------------------------
# Evaluation of time and memory
# -------------------------------------------------------------------------------
rule data_time :
    input :
        expand("{{data_set}}/time/MSA_{msa}_r{region_size}_d{depth}" , msa = MSA, region_size=REGION_SIZES, depth=DEPTHS )
    output :
        "{data_set}/time/all_MSA_time.csv"
    message:
        "Data time formatting for {wildcards.data_set}"
    log:
        "{data_set}/logs/13_data_time.log"
    conda:
        "env_conda/perl.yaml"
    shell:
        'ORDER="./src/data_time.pl {input}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER >{output} 2>>{log}'

# -------------------------------------------------------------------------------
# Meta consensus
# -------------------------------------------------------------------------------
rule separate_consensus :
    input :
        "{data_set}/seq_consensus/seq_consensus_t{threshold}_r{region_size}.fasta"
    output :
        "{data_set}/seq_consensus/separate_by_depth/seq_consensus_t{threshold}_r{region_size}_d{depth}.fasta"
    message:
        "Separate consensus for {wildcards.data_set} (Threshold={wildcards.threshold}, Region size={wildcards.region_size} & Depth={wildcards.depth})"
    log:
        "{data_set}/logs/14_separate_consensus_t{threshold}_r{region_size}_d{depth}.log"
    shell:
        'ORDER="./src/separate_consensus.sh {input} {wildcards.depth} {output}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER >{output} 2>>{log}'

rule msa_consensus :
    input :
        "{data_set}/seq_consensus/separate_by_depth/seq_consensus_t{threshold}_r{region_size}_d{depth}.fasta"
    output :
        time = os.path.join('{data_set}','time','msa_consensus_t{threshold}_r{region_size}_d{depth}'),
        out = os.path.join('{data_set}','meta_consensus','msa_consensus_t{threshold}_r{region_size}_d{depth}.fasta')
    message:
        "MSA Consensus for {wildcards.data_set} (Threshold={wildcards.threshold}, Region size={wildcards.region_size} & Depth={wildcards.depth})"
    log:
        "{data_set}/logs/15_msa_consensus_t{threshold}_r{region_size}_d{depth}.log"
    conda:
        "env_conda/muscle.yaml"
    shell:
        './src/run_MSA.sh "muscle -in {input} -out {output.out}" {input} {output.out} {output.time} {log} 1'

rule meta_consensus:
    input :
        expand('{{data_set}}/meta_consensus/msa_consensus_t{{threshold}}_r{{region_size}}_d{depth}.fasta',depth=DEPTHS)
    output :
        out="{data_set}/meta_consensus/meta_consensus_t{threshold}_r{region_size}.fasta",
        gap="{data_set}/meta_consensus/meta_consensus_t{threshold}_r{region_size}_with_gap.fasta"
    message:
       "Meta consensus for {wildcards.data_set} (Threshold={wildcards.threshold} & Region size={wildcards.region_size})"
    log:
        "{data_set}/logs/16_meta_consensus_t{threshold}_r{region_size}.log"
    conda:
        "env_conda/python3.yaml"
    shell:
        'ORDER="./src/sequence_consensus.py -I -in {input} -t 50 -o {output.out} -g {output.gap}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER >{output} 2>>{log}'

rule alignment_meta_consensus_ref :
    input :
        consensus=os.path.join('{data_set}','meta_consensus','meta_consensus_t{threshold}_r{region_size}.fasta'),
        region="{data_set}/seq_selectes_region/region_seq_r{region_size}.fasta"
    output :
        "{data_set}/meta_consensus/align_meta_consensus_ref_t{threshold}_r{region_size}.txt",
    message:
        "Alignment meta consensus ref for {wildcards.data_set} (Threshold={wildcards.threshold} & Region size={wildcards.region_size})"
    log:
        "{data_set}/logs/17_alignment_meta_consensus_ref_t{threshold}_r{region_size}.log"
    conda:
        "env_conda/exonerate.yaml"
    shell :
        'if [ `cat {input.consensus}|grep ">"|wc -l` -ne 0 ]; then'
        '   ORDER="exonerate --bestn 1 -Q dna -E -m a:g {input.consensus} {input.region}";'
        '   echo "ORDER: $ORDER" >{log};'
        '   $ORDER >{output} 2>>{log};'
        'else'
        '   echo "ERROR: No sequences" >>{log};'
        '   touch {output};'
        'fi'

rule meta_consensus_data_formatting :
    input :
        expand("{{data_set}}/meta_consensus/align_meta_consensus_ref_t{{threshold}}_r{region_size}.txt" , region_size=REGION_SIZES),
    output :
        "{data_set}/results/"+EXP_NAME+"_meta_consensus_data_align_t{threshold}.csv"
    message:
        "Meta consensus data formatting for {wildcards.data_set} (Threshold={wildcards.threshold})"
    log:
        "{data_set}/logs/18_meta_consensus_data_formatting_t{threshold}.log"
    conda:
        "env_conda/python3.yaml"
    shell :
        'ORDER="./src/data_formatting.py -in {input}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER >{output} 2>>{log}'

rule meta_consensus_region_mean:
    input :
        expand("{data_set}/results/"+EXP_NAME+"_meta_consensus_data_align_t{{threshold}}.csv" , data_set = DATA_SETS)
    output :
        EXP + '/'+ EXP_NAME + "/results/"+EXP_NAME+"_meta_consensus_data_align_t{threshold}.csv"
    message:
        "Meta consensus region mean for " + EXP + '/'+ EXP_NAME + " (threshold={wildcards.threshold})"
    log:
        EXP + '/'+EXP_NAME + "/logs/19_meta_consensus_region_mean_t{threshold}.log"
    conda:
        "env_conda/python3.yaml"
    shell :
        'ORDER="./src/region_mean.py -in {input} -out {output} -t {wildcards.threshold}";'
        'echo "ORDER: $ORDER" >{log};'
        '$ORDER 2>>{log}'

rule dataset_stats:
    input :
        os.path.join(EXP,EXP_NAME,'alignement','aln_reads_on_ref.sam')
    output :
        os.path.join(EXP,EXP_NAME,'results','dataset_stats.txt')
    message:
        "Dataset stats for " + EXP + '/'+ EXP_NAME
    log:
        EXP + '/'+EXP_NAME + "/logs/20_dataset_stats.log"
    conda:
        "env_conda/samtools.yaml"
    shell :
        'set +o pipefail;'
        'samtools stats {input} | head -38 >{output} 2>>{log}'
